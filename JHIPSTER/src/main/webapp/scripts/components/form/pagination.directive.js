/* globals $ */
'use strict';

angular.module('tp1taaApp')
    .directive('tp1taaAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
