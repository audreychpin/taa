/* globals $ */
'use strict';

angular.module('tp1taaApp')
    .directive('tp1taaAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
