'use strict';

angular.module('tp1taaApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
