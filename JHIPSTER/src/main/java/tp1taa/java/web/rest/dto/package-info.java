/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package tp1taa.java.web.rest.dto;
