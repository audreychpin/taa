-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 18 Janvier 2016 à 11:00
-- Version du serveur: 5.5.31
-- Version de PHP: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `base_16013929`
--

-- --------------------------------------------------------

--
-- Structure de la table `Developer`
--

CREATE TABLE IF NOT EXISTS `Developer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `Developer`
--

INSERT INTO `Developer` (`id`, `name`) VALUES
(1, 'Jen April'),
(2, 'Philippe Castel'),
(3, 'Jean Alesi');

-- --------------------------------------------------------

--
-- Structure de la table `Epic`
--

CREATE TABLE IF NOT EXISTS `Epic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `Epic`
--

INSERT INTO `Epic` (`id`, `name`) VALUES
(1, 'epopée');

-- --------------------------------------------------------

--
-- Structure de la table `Stakeholder`
--

CREATE TABLE IF NOT EXISTS `Stakeholder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `Stakeholder`
--

INSERT INTO `Stakeholder` (`id`, `name`) VALUES
(1, 'John Smith'),
(2, 'Albert Jean');

-- --------------------------------------------------------

--
-- Structure de la table `Task`
--

CREATE TABLE IF NOT EXISTS `Task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `duration` datetime DEFAULT NULL,
  `developper_id` bigint(20) DEFAULT NULL,
  `userStory_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_r8nxmrcs4lrctqtqq0rpbdpk7` (`developper_id`),
  KEY `FK_78xs7eyw70ih5bsnntwsq1hxp` (`userStory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `Task`
--

INSERT INTO `Task` (`id`, `duration`, `developper_id`, `userStory_id`) VALUES
(1, '2016-01-18 10:52:22', 1, 1),
(2, '2016-01-18 10:52:22', 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `UserStory`
--

CREATE TABLE IF NOT EXISTS `UserStory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estimate` int(11) NOT NULL,
  `informations` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `story` varchar(255) DEFAULT NULL,
  `theme` varchar(255) DEFAULT NULL,
  `epic_id` bigint(20) DEFAULT NULL,
  `stakeholder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_64s6h9a99ijv05cfhsbnkw7hv` (`epic_id`),
  KEY `FK_1bc65knvc6ydkj2ujcu8o0b0m` (`stakeholder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `UserStory`
--

INSERT INTO `UserStory` (`id`, `estimate`, `informations`, `priority`, `story`, `theme`, `epic_id`, `stakeholder_id`) VALUES
(1, 5, 'testtest', 2, 'C''est une belle histoire', 'Contes', 1, 1),
(2, 2, 'Inspiré de Mozart', 3, 'Idée de composition', 'Musique', 1, 2);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Task`
--
ALTER TABLE `Task`
  ADD CONSTRAINT `FK_78xs7eyw70ih5bsnntwsq1hxp` FOREIGN KEY (`userStory_id`) REFERENCES `UserStory` (`id`),
  ADD CONSTRAINT `FK_r8nxmrcs4lrctqtqq0rpbdpk7` FOREIGN KEY (`developper_id`) REFERENCES `Developer` (`id`);

--
-- Contraintes pour la table `UserStory`
--
ALTER TABLE `UserStory`
  ADD CONSTRAINT `FK_1bc65knvc6ydkj2ujcu8o0b0m` FOREIGN KEY (`stakeholder_id`) REFERENCES `Stakeholder` (`id`),
  ADD CONSTRAINT `FK_64s6h9a99ijv05cfhsbnkw7hv` FOREIGN KEY (`epic_id`) REFERENCES `Epic` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
