package services_metiers_jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import jpa.Developer;

public class DeveloperManager {

	public static void createDeveloper(Developer dev, EntityManager manager){
		manager.persist(dev);
	}
	
	public static Developer getDeveloper(Long idDev, EntityManager manager){
		CriteriaBuilder cb = manager.getCriteriaBuilder();		
		CriteriaQuery<Developer> q = cb.createQuery(Developer.class);
		Root<Developer> root = q.from(Developer.class);
		q.where(cb.equal(root.get("id"), idDev));
		return manager.createQuery(q).getSingleResult();
	}
	
	public static Developer updateDeveloper(Developer dev, EntityManager manager){
		return manager.merge(dev);
	}
	
	public static void deleteDeveloper(Developer dev, EntityManager manager){
		manager.remove(dev);
	}
	
	public static List<Developer> getListDevelopers(EntityManager manager){
		List<Developer> resultList = manager
				.createQuery("SELECT dev FROM Developer dev",Developer.class).getResultList();
		return resultList;
	}
}
