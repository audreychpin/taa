package services_metiers_jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import jpa.UserStory;

public class UserStoryManager {
	public static void createUserStory(UserStory us, EntityManager manager){
		manager.persist(us);
	}
	
	public static UserStory getUserStory(Long idUS, EntityManager manager){
		CriteriaBuilder cb = manager.getCriteriaBuilder();		
		CriteriaQuery<UserStory> q = cb.createQuery(UserStory.class);
		Root<UserStory> root = q.from(UserStory.class);
		q.where(cb.equal(root.get("id"), idUS));
		return manager.createQuery(q).getSingleResult();
	}
	
	public static UserStory updateUserStory(UserStory us, EntityManager manager){
		return manager.merge(us);
	}
	
	public static void deleteUserStory(UserStory us, EntityManager manager){
		manager.remove(us);
	}
	
	public static List<UserStory> getListUserStories(EntityManager manager){
		List<UserStory> resultList = manager
				.createQuery("SELECT us FROM UserStory us",UserStory.class).getResultList();
		return resultList;
	}
}