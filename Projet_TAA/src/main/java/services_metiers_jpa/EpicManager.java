package services_metiers_jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import jpa.Epic;

public class EpicManager {

	public static void createEpic(Epic epic, EntityManager manager) {
		manager.persist(epic);
	}
	
	public static Epic getEpic(Long idEpic, EntityManager manager){
		CriteriaBuilder cb = manager.getCriteriaBuilder();		
		CriteriaQuery<Epic> q = cb.createQuery(Epic.class);
		Root<Epic> root = q.from(Epic.class);
		q.where(cb.equal(root.get("id"), idEpic));
		return manager.createQuery(q).getSingleResult();
	}
	
	public static Epic updateEpic(Epic epic, EntityManager manager){
		return manager.merge(epic);
	}
	
	public static void deleteEpic(Epic epic, EntityManager manager){
		manager.remove(epic);
	}
	
	public static List<Epic> getListEpics(EntityManager manager){
		List<Epic> resultList = manager
				.createQuery("SELECT epic FROM Epic epic",Epic.class).getResultList();
		return resultList;
	}

}
