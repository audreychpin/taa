package services_metiers_jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import jpa.Stakeholder;

public class StakeholderManager {

	public static void createStakeholder(Stakeholder sh, EntityManager manager) {
		manager.persist(sh);
	}
	
	public static Stakeholder getStakeholder(Long idStakeholder, EntityManager manager){
		CriteriaBuilder cb = manager.getCriteriaBuilder();		
		CriteriaQuery<Stakeholder> q = cb.createQuery(Stakeholder.class);
		Root<Stakeholder> root = q.from(Stakeholder.class);
		q.where(cb.equal(root.get("id"), idStakeholder));
		return manager.createQuery(q).getSingleResult();
	}
	
	public static Stakeholder updateStakeholder(Stakeholder sh, EntityManager manager){
		return manager.merge(sh);
	}
	
	public static void deleteStakeholder(Stakeholder sh, EntityManager manager){
		manager.remove(sh);
	}
	
	public static List<Stakeholder> getListStakeholders(EntityManager manager){
		List<Stakeholder> resultList = manager
				.createQuery("SELECT sh FROM Stakeholder sh",Stakeholder.class).getResultList();
		return resultList;
	}

}
