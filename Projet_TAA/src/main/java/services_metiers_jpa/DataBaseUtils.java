package services_metiers_jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DataBaseUtils {
	private static EntityManager manager;
	private static EntityManagerFactory factory;
	private static EntityTransaction tx;
	
	public static void initConnection(){
		factory = Persistence.createEntityManagerFactory("dev");
	}

    public static EntityManager getEntityManager() {
    	initConnection();
        if (manager == null) {
        	manager = factory.createEntityManager();
        }
        return manager;
    }
	
	public static void beginTransaction(){
		tx = manager.getTransaction();
		tx.begin();
	}
	
	public static void commitTransaction(){
		tx.commit();
	}
	
	public static void rollbackTransaction(){
		tx.rollback();
	}
	
	public static void closeConnection(){
		manager.close();
		factory.close();
	}
}