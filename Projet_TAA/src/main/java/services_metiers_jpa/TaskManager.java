package services_metiers_jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import jpa.Task;

public class TaskManager {

	public static void createTask(Task task, EntityManager manager){
		manager.persist(task);
	}
	
	public static Task getTask(Long idtask, EntityManager manager){
		CriteriaBuilder cb = manager.getCriteriaBuilder();		
		CriteriaQuery<Task> q = cb.createQuery(Task.class);
		Root<Task> root = q.from(Task.class);
		q.where(cb.equal(root.get("id"), idtask));
		return manager.createQuery(q).getSingleResult();
	}
	
	public static Task updateTask(Task task, EntityManager manager){
		return manager.merge(task);
	}
	
	public static void deleteTask(Task task, EntityManager manager){
		manager.remove(task);
	}
	
	public static List<Task> getListTasks(EntityManager manager){
		List<Task> resultList = manager
				.createQuery("SELECT task FROM Task task",Task.class).getResultList();
		return resultList;
	}
}
