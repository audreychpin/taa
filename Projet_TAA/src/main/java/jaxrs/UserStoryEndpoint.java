/**
 * JBoss, Home of Professional Open Source
 * Copyright Red Hat, Inc., and individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jaxrs;
	
import java.util.List;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import jpa.UserStory;
import services_metiers_jpa.DataBaseUtils;
import services_metiers_jpa.UserStoryManager;

@Path("/userstory")
public class UserStoryEndpoint {

    @GET
    public Response getStatus() {
        return Response.status(Response.Status.OK).entity("JO").build();
    }
    
    /**
     * Méthode pour récupérer une UserStory à partir de son id
     * @param idUS
     * @return
     */
    @GET
    @Path("/get")
    @Consumes(MediaType.APPLICATION_JSON)
    public UserStory getUserStory(Long idUS) {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On récupère l'enregistrement
    	UserStory us = UserStoryManager.getUserStory(idUS, manager);
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    	// On retourne le résultat
        return us;
    }
    
    /**
     * Méthode pour mettre à jour une UserStory
     * @param us
     */
    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateUserStory(UserStory us) {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On démarre notre transaction
    	DataBaseUtils.beginTransaction();
    	// On met à jour l'enregistrement
    	UserStoryManager.updateUserStory(us, manager);
    	// On commite la transaction
    	DataBaseUtils.commitTransaction();
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    }
    
    /**
     * Méthode pour supprimer une UserStory à partir de son id
     * @param idUS
     */
    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteUserStory(Long idUS) {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On démarre notre transaction
    	DataBaseUtils.beginTransaction();
    	// On supprime l'enregistrement
    	UserStoryManager.deleteUserStory(UserStoryManager.getUserStory(idUS, manager), manager);
    	// On commite la transaction
    	DataBaseUtils.commitTransaction();
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    }
    
    /**
     * Méthode pour créer une UserStory
     * @param us
     */
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void createUserStory(UserStory us) {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On démarre notre transaction
    	DataBaseUtils.beginTransaction();
    	// On crée l'enregistrement
    	UserStoryManager.createUserStory(us, manager);
    	// On commite la transaction
    	DataBaseUtils.commitTransaction();
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    }
    
    /**
     * Méthode qui liste toutes les UserStory en base de données
     * @return
     */
    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserStory> getAll() {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On récupère les enregistrements
    	List<UserStory> listUS = UserStoryManager.getListUserStories(manager);
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    	// On retourne les résultats
    	return listUS;
    }
}