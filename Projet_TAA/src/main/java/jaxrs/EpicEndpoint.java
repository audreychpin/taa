/**
 * JBoss, Home of Professional Open Source
 * Copyright Red Hat, Inc., and individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jaxrs;
	
import java.util.List;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import jpa.Epic;
import services_metiers_jpa.DataBaseUtils;
import services_metiers_jpa.EpicManager;

@Path("/epic")
public class EpicEndpoint {

    @GET
    public Response getStatus() {
        return Response.status(Response.Status.OK).entity("JO").build();
    }
    
    /**
     * Méthode pour récupérer une Epic à partir de son id
     * @param idUS
     * @return
     */
    @GET
    @Path("/get")
    @Consumes(MediaType.APPLICATION_JSON)
    public Epic getEpic(Long idEpic) {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On récupère l'enregistrement
    	Epic epic = EpicManager.getEpic(idEpic, manager);
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    	// On retourne le résultat
        return epic;
    }
    
    /**
     * Méthode pour mettre à jour une Epic
     * @param epic
     */
    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateEpic(Epic epic) {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On démarre notre transaction
    	DataBaseUtils.beginTransaction();
    	// On met à jour l'enregistrement
    	EpicManager.updateEpic(epic, manager);
    	// On commite la transaction
    	DataBaseUtils.commitTransaction();
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    }
    
    /**
     * Méthode pour supprimer une Epic à partir de son id
     * @param idUS
     */
    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteEpic(Long idEpic) {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On démarre notre transaction
    	DataBaseUtils.beginTransaction();
    	// On supprime l'enregistrement
    	EpicManager.deleteEpic(EpicManager.getEpic(idEpic, manager), manager);
    	// On commite la transaction
    	DataBaseUtils.commitTransaction();
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    }
    
    /**
     * Méthode pour créer une Epic
     * @param epic
     */
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void createEpic(Epic epic) {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On démarre notre transaction
    	DataBaseUtils.beginTransaction();
    	// On crée l'enregistrement
    	EpicManager.createEpic(epic, manager);
    	// On commite la transaction
    	DataBaseUtils.commitTransaction();
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    }
    
    /**
     * Méthode qui liste toutes les Epics en base de données
     * @return
     */
    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Epic> getAll() {
    	// On initialise la connection et on récupère le manager
    	EntityManager manager = DataBaseUtils.getEntityManager();
    	// On récupère les enregistrements
    	List<Epic> listUS = EpicManager.getListEpics(manager);
    	System.out.println("testtest");
    	// On ferme la connection
    	DataBaseUtils.closeConnection();
    	// On retourne les résultats
    	return listUS;
    }
}