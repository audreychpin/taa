package jpa;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Task {
	private Long id;
	private UserStory userStory;
	private Developer developper;
	private Date duration;
	
	public Task(){}
	
	public Task(UserStory userStory, Developer developper, Date duration) {
		this.userStory = userStory;
		this.developper = developper;
		this.duration = duration;
	}	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne
	public UserStory getUserStory() {
		return userStory;
	}
	public void setUserStory(UserStory userStory) {
		this.userStory = userStory;
	}
	@ManyToOne
	public Developer getDevelopper() {
		return developper;
	}
	public void setDevelopper(Developer developper) {
		this.developper = developper;
	}
	public Date getDuration() {
		return duration;
	}
	public void setDuration(Date duration) {
		this.duration = duration;
	}	
}