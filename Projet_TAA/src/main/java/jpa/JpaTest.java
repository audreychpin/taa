package jpa;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import services_metiers_jpa.DataBaseUtils;
import services_metiers_jpa.DeveloperManager;
import services_metiers_jpa.EpicManager;
import services_metiers_jpa.StakeholderManager;
import services_metiers_jpa.TaskManager;
import services_metiers_jpa.UserStoryManager;

public class JpaTest {
    private static final Logger logger = Logger.getLogger(JpaTest.class.getName());

	public static void main(String[] args) {
		//Initialisation des éléments liés à la base de données
		EntityManager manager = DataBaseUtils.getEntityManager();

		logger.info("----------- insertions------------");
		
		//Début de la transaction
		DataBaseUtils.beginTransaction();
		//insertions
		insertTest(manager);
		//fin de la transaction
		DataBaseUtils.commitTransaction();		
		//affichage des résultats
		affichageTest(manager);
		
		logger.info("----------- modifications------------");
		
		//modifications
		DataBaseUtils.beginTransaction();
		updateTest(manager);
		DataBaseUtils.commitTransaction();		
		affichageTest(manager);
		
		logger.info("----------- suppressions------------");
		
		//suppressions
		DataBaseUtils.beginTransaction();
		deleteTest(manager);
		DataBaseUtils.commitTransaction();		
		affichageTest(manager);
		
		DataBaseUtils.closeConnection();
	}
	
	private static void deleteTest(EntityManager manager) {
		Task task = TaskManager.getTask(new Long(3), manager);
		TaskManager.deleteTask(task, manager);
	}

	private static void updateTest(EntityManager manager) {
		UserStory us = UserStoryManager.getUserStory(new Long(1), manager);
		us.setInformations("testtest");
		UserStoryManager.updateUserStory(us, manager);
		
		Developer dev = new Developer("Jean Alesi");
		DeveloperManager.createDeveloper(dev, manager);
		
		Task task = TaskManager.getTask(new Long(2), manager);
		task.setDevelopper(dev);
		TaskManager.updateTask(task, manager);		
	}

	private static void insertTest(EntityManager manager){
		try {
			//insertions
			Epic epic = new Epic("epopée");
			EpicManager.createEpic(epic, manager);

			Stakeholder stakeholder = new Stakeholder("John Smith");
			StakeholderManager.createStakeholder(stakeholder, manager);

			Stakeholder stakeholder2 = new Stakeholder("Albert Jean");
			StakeholderManager.createStakeholder(stakeholder2, manager);
			
			UserStory us = new UserStory("C'est une belle histoire",
					2, 5, "", "Contes", epic, stakeholder);
			UserStoryManager.createUserStory(us, manager);
			
			UserStory us2 = new UserStory("Idée de composition",
					3, 2, "Inspiré de Mozart", "Musique", epic, stakeholder2);
			UserStoryManager.createUserStory(us2, manager);
			
			Developer dev = new Developer("Jen April");
			DeveloperManager.createDeveloper(dev, manager);
			
			Developer dev2 = new Developer("Philippe Castel");
			DeveloperManager.createDeveloper(dev2, manager);
			
			Task task = new Task(us, dev, new Date());
			TaskManager.createTask(task, manager);
			
			Task task2 = new Task(us, dev2, new Date());
			TaskManager.createTask(task2, manager);
			
			Task task3 = new Task(us2, dev2, new Date());
			TaskManager.createTask(task3, manager);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void affichageTest(EntityManager manager){
		List<UserStory> userStories = UserStoryManager.getListUserStories(manager);
		for (UserStory userStory : userStories) {
			System.out.println(userStory.getId() + " : " + userStory.getStory());
		}
		
		List<Task> tasks = TaskManager.getListTasks(manager);
		for (Task task : tasks) {
			System.out.println(task.getId() + " by " + task.getDevelopper().getName());
		}
	}
}