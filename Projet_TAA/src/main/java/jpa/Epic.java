package jpa;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Epic{
	private Long id;
	private String name;
	private Set<UserStory> userStories;
	
	public Epic(){}
	
	public Epic(String name){
		this.name = name;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@OneToMany(mappedBy="epic")
	public Set<UserStory> getUserStories() {
		return userStories;
	}
	public void setUserStories(Set<UserStory> userStories) {
		this.userStories = userStories;
	}
}