'use strict';

/**
 * @ngdoc function
 * @name projetAngularJsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the projetAngularJsApp
 */
angular.module('projetAngularJsApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
