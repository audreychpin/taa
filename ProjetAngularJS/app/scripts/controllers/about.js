'use strict';

/**
 * @ngdoc function
 * @name projetAngularJsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the projetAngularJsApp
 */
angular.module('projetAngularJsApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
