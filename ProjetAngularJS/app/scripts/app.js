'use strict';

/**
 * @ngdoc overview
 * @name projetAngularJsApp
 * @description
 * # projetAngularJsApp
 *
 * Main module of the application.
 */
var projetAngularJsApp = angular.module('projetAngularJsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

projetAngularJsApp.controller('contentCtrl', ['$scope',
     function ($scope) {

         // Pour manipuler plus simplement les userStories au sein du contrôleur
         // On initialise les userStories avec un tableau vide : []
         var userStories = $scope.userStories = [];
         
		//Ajouter une userStory
         $scope.addUserStory = function () {
        	 // on récupère l'histoire
        	 var story = $scope.story.trim();
        	 //on récupère la priorité
        	 var priority = $scope.priority;
        	 //on récupère l'estimation
        	 var estimate = $scope.estimate;
        	 // on récupère les informations
        	 var informations = $scope.informations.trim();
        	 // on récupère le thème
        	 var theme = $scope.theme.trim();
        	 //on récupère l'épopée
        	 var epic = $scope.epic;
        	 //on récupère le stakeholder
        	 var stakeholder = $scope.stakeholder;
        	 if (!story.length || !priority.length || !estimate.length || !informations.length || !theme.length
        			 || !epic.length || !stakeholder.length) {
        		 // éviter les story vides
        		 return;
        	 }
        	 
        	 userStories.push({
        		 // on ajoute la userStory au tableau des userStories
        		 story: story,
        		 priority: priority,
        		 estimate: estimate,
        		 informations: informations,
        		 theme: theme,
        		 epic: epic,
        		 stakeholder: stakeholder,
        		 completed: false
        	 });
        	 // Réinitialisation des variables
        	 $scope.story = '';
        	 $scope.priority = '';
        	 $scope.estimate = '';
        	 $scope.informations = '';
        	 $scope.theme = '';
        	 $scope.epic = '';
        	 $scope.stakeholder = '';
         };
		         
		//Enlever une userStory
         $scope.removeUserStory = function (userStory) {
        	 userStories.splice(userStories.indexOf(userStory), 1);
         };
		         
		// Cocher / Décocher toutes les userStories
         $scope.markAll = function (completed) {
        	 userStories.forEach(function (userStory) {
        		 userStory.completed = !completed;
        	 });
         };
		         
		// Enlever toutes les userStories cochées
         $scope.clearCompletedUserStories = function () {
        	 $scope.userStories = userStories = userStories.filter(function (userStory) {
        		 return !userStory.completed;
        	 });
         };
	}
 ]);

/*projetAngularJsApp.controller('testCTRL', ['$scope', '$http',
      function ($scope, $http) {
		$scope.epics = {};
		
		$scope.epics = User.query();

		$scope.test = function () {
			alert('test');
         		 $http({
         			 method: 'GET',
         			 url: 'http://localhost:8080/epic/',
         			 headers: {'Content-Type': 'application/json'},
         			 data:  $scope.epics
         		 }).success(function (data) {
         			 $scope.epics=data;
         			 alert(epics[1].name);
 				 });
         	 };
          }
		

		$scope.test = function($resource) {
			
			epics = $resource('http://localhost:8080/epic/');
			alert(epics[1].name);
			
		    $http({method : 'GET',url : 'http://localhost:8080/epic/', headers: { 'X-Parse-Application-Id':'XXXXXXXXXXXXX', 'X-Parse-REST-API-Key':'YYYYYYYYYYYYY'}})
		       .success(function(data, status) {
		    	   $scope.epics=data;
       			 	alert(epics[1].name);
		        }).error(function(data, status) {
		           alert("Error");
		       });
		   };
		 
		}

  ]);

angular.module('projetAngularJsApp', ['ngResource'])
.factory('User', ['$resource',
  function($resource){
    return $resource('http://localhost:8080/epic/');
  }]
);*/